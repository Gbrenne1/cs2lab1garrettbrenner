package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

public class Inventory {

	private static final String NAME_CANNOT_BE_NULL = "Dealership cannot be null";
	private static final String NAME_CANNOT_BE_EMPTY = "Dealrship cannot be empty";
	private static final String AUTOMOBILE_CANNOT_BE_NULL = "Automobile cannot be null.";
	private static final int HIGHEST_YEAR_BOUND = 3000;
	private static final int LOWEST_YEAR_BOUND = 1885;

	
	private String name;
	private ArrayList<Automobile> inventory;
	
	/**
	 * Default constructor
	 * 
	 * @precondition none
	 * 
	 * @postcondition none
	 * 
	 */
	
	public Inventory() {
		this.name = new String();
		this.inventory = new ArrayList<Automobile>();
	}
	
	/**
	 * Instantiates a new dealership and list of cars
	 * 
	 * @Precondition dealership != null && dealership is not empty; getAutomobiles() == autos;
	 */
	
	public Inventory(String dealership) {
		if(dealership == null) {
			throw new IllegalArgumentException(NAME_CANNOT_BE_NULL);
		}
		if(dealership.isEmpty()) {
			throw new IllegalArgumentException(NAME_CANNOT_BE_EMPTY);
		}
		
		this.name = dealership;
		this.inventory = new ArrayList<Automobile>();
	}
	
	/**
	 * get the dealership
	 * @return the dealership
	 */
	public String getDealership() {
		return name;
	}

	/**
	 * set the dearship
	 * @param dealership the dealership to set
	 */
	public void setDealership(String dealership) {
		this.name = dealership;
	}

	/**
	 * get the automobile collection
	 * @return the automobiles
	 */
	public ArrayList<Automobile> getAutomobiles() {
		return inventory;
	}
	
	/**
	 * gets the size of the automobile collection
	 * 
	 *  @precondition none
	 *  @postcondition none
	 *  
	 *  @return automobiles collection size
	 */
	public int getSize() {
		return this.inventory.size();
	}
	
	/**
	 * Adds automobile to inventory list 
	 * 
	 * @precondition automobile != null;
	 * @postcondition inventory.size = inventory.sie + 1;
	 * 
	 * @Param auto the automobile being added to the list
	 * 
	 * @return True or False based on in car is successfully added 
	 */
	public boolean addToInventory(Automobile auto) {
		if(auto == null) {
			throw new IllegalArgumentException(AUTOMOBILE_CANNOT_BE_NULL);
		}
		this.inventory.add(auto);
		return true;
	}
	
	/**
	 * returns the year of the oldest automobile in the inventory
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return year of the oldest car in the inventory
	 */
	public int getOldestCar() {
		int year = HIGHEST_YEAR_BOUND;
		for(Automobile auto : this.inventory) {
			if(auto.getYear() < year) {
				year = auto.getYear();
			}
		}
		return year;
	}
	
	/**
	 * returns the year of the newest car in the inventory 
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return year of the newest car in the inventory
	 */
	public int getNewestCar() {
		int year = LOWEST_YEAR_BOUND;
		for(Automobile auto : this.inventory) {
			if(auto.getYear() > year) {
				year = auto.getYear();
			}
		}
		return year;
	}
	
	/**
	 * returns number of cars between a certain age range
	 * 
	 * @precondition earlyYear >= LOWEST_YEAR_BOUND; latestYear <= HIGHEST_YEAR_BOUND;
	 * @postcondition none
	 * 
	 * @param earlyYear the earliest date for the date range
	 * @param latestYear the latest date for the date range
	 * 
	 *  @return the number of cars between the set date range
	 */
	public int getCarsBetweenDateRange(int earlyYear, int latestYear) {
		if(earlyYear < LOWEST_YEAR_BOUND) {
			throw new IllegalArgumentException("year must be greater than 1885.");
		}
		if(latestYear > HIGHEST_YEAR_BOUND) {
			throw new IllegalArgumentException("year must be less than 3000.");
		}
		int carsBetweenDates = 0;
		for(Automobile auto : this.inventory) {
			if(auto.getYear() >= earlyYear && auto.getYear() <= latestYear) {
				carsBetweenDates += 1;
			}
		}
		return carsBetweenDates;
	}
	
	/**
	 * returns a specified string 
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return string containing the name of the dealership and the number of cars in the inventory
	 */
	public String toString() {
		String string = "Dealership: " + this.name + " " + "#Autos: " + this.inventory.size();
		return string;
	}
}

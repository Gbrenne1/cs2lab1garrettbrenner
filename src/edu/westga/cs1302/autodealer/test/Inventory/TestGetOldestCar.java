package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestGetOldestCar {

	@Test
	void testGetOldestCarInEmptyList() {
		Inventory inv = new Inventory();
		assertEquals(3000, inv.getOldestCar());
	}

	@Test
	void testGetOldestCarInInventoryOfOne() {
		Inventory inv = new Inventory();
		Automobile auto = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		inv.addToInventory(auto);
		assertEquals(2013, inv.getOldestCar());
	}
	
	@Test
	void testGetOldestCarFirstInList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto2);
		inv.addToInventory(auto1);
		inv.addToInventory(auto3);
		assertEquals(1997, inv.getOldestCar());
	}
	
	@Test
	void testGetOldestCarMiddleOfList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		inv.addToInventory(auto3);
		assertEquals(1997, inv.getOldestCar());
	}
	
	@Test
	void testGetOldestCarLastInList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto3);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		assertEquals(1997, inv.getOldestCar());
	}
}

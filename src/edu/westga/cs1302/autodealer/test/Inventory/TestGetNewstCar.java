package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestGetNewstCar {

	@Test
	void testGetNewestCarInEmptyList() {
		Inventory inv = new Inventory();
		assertEquals(1885, inv.getNewestCar());
	}

	@Test
	void testGetNewestCarInInventoryOfOne() {
		Inventory inv = new Inventory();
		Automobile auto = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		inv.addToInventory(auto);
		assertEquals(2013, inv.getNewestCar());
	}
	
	@Test
	void testGetNewestCarFirstInList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto3);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		assertEquals(2020, inv.getNewestCar());
	}
	
	@Test
	void testGetNewestCarMiddleOfList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto1);
		inv.addToInventory(auto3);
		inv.addToInventory(auto2);
		assertEquals(2020, inv.getNewestCar());
	}
	
	@Test
	void testGetNewestCarLastInList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123, 123);
		Automobile auto3 = new Automobile("Subaru", "WRX", 2020, 123, 123);
		inv.addToInventory(auto2);
		inv.addToInventory(auto1);
		inv.addToInventory(auto3);
		assertEquals(2020, inv.getNewestCar());
	}

}

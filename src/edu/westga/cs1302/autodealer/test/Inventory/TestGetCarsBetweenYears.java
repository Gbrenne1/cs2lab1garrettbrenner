package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestGetCarsBetweenYears {

	@Test
	void testInvalidYearInput() {
		Inventory inv = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inv.getCarsBetweenDateRange(1884, 3001));
	}
	
	@Test
	void testOneCarOutOfDateRange() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		inv.addToInventory(auto1);
		assertEquals(0, inv.getCarsBetweenDateRange(2014, 2020));
	}
	
	@Test
	void testOneCarBetweenDateRange() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		inv.addToInventory(auto1);
		assertEquals(1, inv.getCarsBetweenDateRange(2012, 2020));
	}
	
	@Test
	void testTwoCarsInListOneInDateRange() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123,123);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		assertEquals(1, inv.getCarsBetweenDateRange(2012, 2020));
	}
	
	@Test
	void testTwoCarsInListTwoInDateRange() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123,123);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		assertEquals(2, inv.getCarsBetweenDateRange(1996, 2020));
	}
	
}

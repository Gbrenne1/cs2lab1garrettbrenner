package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;
import edu.westga.cs1302.autodealer.test.TestingConstants;

class TestSingleParamConstructor {

	@Test
	void testValidConstruction() {
		Inventory inventory = new Inventory("Subaru");

		assertAll(
				() -> assertEquals("Subaru", inventory.getDealership()),
				() -> assertEquals(0, inventory.getSize())
				);	
	}
	
	@Test
	void testNullDealerName() {{
		assertThrows(IllegalArgumentException.class, () -> new Inventory(null));
		}
	}
	
	@Test
	void testEmptyDealershipName() {
		assertThrows(IllegalArgumentException.class, () -> new Inventory(""));
	}
}

package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;
import edu.westga.cs1302.autodealer.test.TestingConstants;

class TestDefaultConstructor {

	@Test
	void testValidConstruction() {
		Inventory inventory = new Inventory();

		assertAll(
				() -> assertEquals("", inventory.getDealership()),
				() -> assertEquals(0,inventory.getSize())
				);
				}

}

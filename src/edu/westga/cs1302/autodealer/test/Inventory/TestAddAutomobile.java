package edu.westga.cs1302.autodealer.test.Inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAddAutomobile {

	@Test
	void testNullAutomobile() {
		Automobile auto = null;
		Inventory inv = new Inventory("Subaru");
		assertThrows(IllegalArgumentException.class, () -> inv.addToInventory(auto));
	}

	@Test
	void testAddingToEmptyList() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		inv.addToInventory(auto1);
		assertEquals(1, inv.getSize());
	}
	
	@Test
	void testAddingToListWithOtherCars() {
		Inventory inv = new Inventory();
		Automobile auto1 = new Automobile("Subaru", "BRZ", 2013, 123, 123);
		Automobile auto2 = new Automobile("Subaru", "Forester", 1997, 123,123);
		inv.addToInventory(auto1);
		inv.addToInventory(auto2);
		assertEquals(2, inv.getSize());
	}
}
